import {
  createAppContainer,
  createSwitchNavigator,
  createStackNavigator,
  createDrawerNavigator
} from "react-navigation";

import Login from "./pages/Login";
import Evento from "./pages/Evento";
import EventoDetalhe from "./pages/EventoDetalhe";

const DrawerNavigator = createDrawerNavigator(
  {
    Inicio: EventoDetalhe,
    VoltarEvento: Evento
  },
  { headerMode: "none" }
);
const NormalNavigator = createStackNavigator(
  {
    Evento,
    Login
  },
  { headerMode: "none" }
);

const AppNavigator = createSwitchNavigator({
  Evento: NormalNavigator,
  EventoDetalhe: DrawerNavigator
});

export default createAppContainer(AppNavigator);

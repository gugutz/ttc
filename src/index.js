import React from "react";
import { YellowBox } from "react-native";
import { Provider as PaperProvider } from "react-native-paper";
YellowBox.ignoreWarnings(["Unrecognized Websocket"]);

import Routes from "./routes";

export default function App() {
  return (
    <PaperProvider>
      <Routes />
    </PaperProvider>
  );
}

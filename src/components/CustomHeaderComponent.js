import React, { Component } from "react";

import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
export default class CustomHeaderComponent extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.leftNavbar}>Eventos UNIVALI</Text>
        <Icon style={styles.rightNavbar} name="person" size={30} color="#900" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#7AC36A"
  }
});

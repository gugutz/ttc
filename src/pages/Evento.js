import React, { useEffect, useState } from "react";

import {
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  ScrollView
} from "react-native";

import {
  Appbar,
  Searchbar,
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  IconButton
} from "react-native-paper";

export default function Evento({ navigation }) {
  handleLogin = () => {
    navigation.navigate("Login");
  };

  handleEventoDetalhe = () => {
    navigation.navigate("EventoDetalhe");
  };

  var firstQuery = "";
  var mock = ["1", "2", "3", "4", "5", "6"];
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <Appbar.Header style={styles.headerBar}>
          <Appbar.Content style={{ color: "#fff" }} title="Eventos UNIVALI" />
          <Appbar.Action icon="account" onPress={this.handleLogin} />
          {/* <Appbar.Action icon="dots-vertical" onPress={this._handleMore} /> */}
        </Appbar.Header>
        <Searchbar
          style={styles.busca}
          placeholder="Procure por um evento"
          value={firstQuery}
        />
      </View>
      <ScrollView>
        {mock.map(function(item, index) {
          return (
            <Card
              key={index}
              style={styles.cards}
              onPress={this.handleEventoDetalhe}
            >
              <Card.Title
                style={{ minHeight: 120 }}
                title="Evento de dar o cu"
                subtitle="Quem mais der o cu ganha"
                leftStyle={{ minWidth: 100 }}
                left={() => (
                  <Image
                    style={{ width: 100, height: 100 }}
                    source={{ uri: "https://picsum.photos/100" }}
                  />
                )}
              />
            </Card>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#C0CCDA"
  },
  headerBar: {
    elevation: 0,
    backgroundColor: "#C0CCDA"
  },
  cards: {
    borderBottomWidth: 1,
    borderBottomColor: "#cdcdcd",
    minHeight: 120,
    borderRadius: 0
  },
  busca: {
    // elevation: 0,
    margin: 10
  }
});
